unsigned int mot_passe[6] = {600,300,300,600,600};
unsigned long temps_courant = millis();
unsigned long dernier_temps = millis();
unsigned int seuil = 10;
unsigned int indexpass = 0;
boolean tap = false;

unsigned long rythme_temps = millis();

#include <Servo.h>

Servo myservo1;
Servo myservo2;

#define CLOSEPOS1 80
#define CLOSEPOS2 180
#define OPENPOS1 170
#define OPENPOS2 90

void setup() {
  myservo1.attach(3);
  myservo2.attach(6);
  pinMode(13, OUTPUT);
  indexpass = 0;
  Serial.begin(9600);
        myservo1.write(CLOSEPOS1);   //Fermer 
        myservo2.write(CLOSEPOS2);   //Fermer
}


bool test_interval(unsigned int tolerance, unsigned int indexpass, unsigned int difference_temps) {
  Serial.print("indexpass:");
  Serial.print(indexpass);
  Serial.print("  tab:");
  Serial.print(mot_passe[indexpass]);
  Serial.print("  diff:");
  Serial.println(difference_temps);
 if(mot_passe[indexpass] + tolerance > difference_temps)
 {
    Serial.println("debug A");
    if( mot_passe[indexpass] - tolerance < difference_temps )
    {
      Serial.println("debug B");
      return true;
    }
 }
  return false;
}

void reinitialise(){
  indexpass = 0;
}

void loop() {
  unsigned int piezo_valeur = analogRead(A0);
  //analogWrite(13, map(piezo_valeur,0,50,0,255));
  //Serial.println(piezo_valeur);
  if(piezo_valeur > seuil && tap == false){
    //Serial.println("ici");
    tap = true;
    dernier_temps = temps_courant;
    temps_courant = millis();

    unsigned long difference = temps_courant - dernier_temps;
    //Serial.println(difference);
    if (test_interval(150,indexpass,difference)==true){
      //ici les delais sont bon 
      Serial.println(difference);
      indexpass = indexpass + 1;
      if (indexpass == 5) {
        //Gagné, on ouvre la boite 
        Serial.println("WIN");  
        indexpass = 0;
        myservo1.write(OPENPOS1); //ouvrir
        myservo2.write(OPENPOS2); //ouvrir
        
        delay(15000); //10 sec
        myservo1.write(CLOSEPOS1);   //Fermer 
        myservo2.write(CLOSEPOS2);   //Fermer
      }
    } else {
      //Ici le  delais sont mauvais, l'utilisateur ne tappe pas le bon rythme
      reinitialise();
      Serial.println("LOOSE");
      
    }
    delay(200);
    tap = false;
  } else {
    if(millis()-rythme_temps>5000)
    {
      rythme_temps=millis();
      digitalWrite(13,HIGH);
    }
    else if(millis()-rythme_temps>2500)
    {
      digitalWrite(13,LOW);
    }
   
  }
}

