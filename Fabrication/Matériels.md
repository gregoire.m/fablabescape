|        Type       |   Nom                                             | Qté    |
| :-----------:     | :------                                           | ---:   |
|        :          | Contreplaqué de peuplié (400x600x5)               | x19    |
|   **Plaques**     | PMMA (400x600x5)                                  | x1     |
|        :          | PMMA (400x600x3)                                  | x1     |
|-------------------|---------------------------------------------------|--------|
|        :          | Arduino Uno                                       | x2     |
|        :          | Touch Pad                                         | x1     |
|        :          | Timer                                             | x1     |
|        :          | LED verte                                         | x4     |
|        :          | LED jaune                                         | x1     |
|        :          | Servo moteur                                      | x2     |
|        :          | Capteur piezo                                     | x1     |
|        :          | Résistance 220O                                   | x2     |
|        :          | Résistance 1Mo                                    | x1     |
| **Electronique**  | Carte à trou 2.54                                 |        |
|        :          | Conecteur mâle 2.54                               |        |
|        :          | Câblage & matériel à souder                       |        |
|        :          | Lampe 230V                                        | x1     |
|        :          | Clé USB                                           | x2     |
|        :          | Ralonge USB >30cm                                 | x1     |
|        :          | Multiprise                                        | x1     |
|        :          | Câble d'alimentation Arduino                      | x2     |
|        :          | Chargeur Arduino                                  | x2     |
|        :          | Ordinateur                                        | x1     |
|-------------------|---------------------------------------------------|--------|
|         :         | Boulon (4mm)                                      | x14    |
| **Quincaillerie** | Boulon tête plate (6mm)                           | x1     |
|         :         | Ecrou                                             | x10    |
|         :         | Insert                                            | x7     |
|-------------------|---------------------------------------------------|--------|
|         :         | Colle à bois                                      | x1     |
| **Adhésif**       | Colle époxy                                       | x1     |
|         :         | Colle superglue                                   | x1     |
|         :         | Scotch transparent                                | x1     |
|-------------------|---------------------------------------------------|--------|
|         :         | Papier blanc A4                                   | x4     |
|         :         | Transparent imprimable A4                         | x1     |
| **Autre**         | Fiole + bouchon                                   | x4     |
|         :         | Colorant alimentaire (bleu, rouge, jaune, vert)   | x4     |
|         :         | Vinyle bleu                                       |        |
|         :         | Patin                                             | x3     |