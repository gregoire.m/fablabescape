# fablabescape

FabLab Escape est un escape game nomade qui a pour but de faire découvrir l'univers des FabLabs. Vous trouverez ici la documentation nécessaire pour la fabrication, mais aussi des conseils pour la mise en place et l'animation du jeu.

La documentation est encore en cours de création et sera disponible avant 2019.